﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Keywest.Models
{
    public class UserEntity
    {
        public string Id { get; set; }
        public string Contact { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyId { get; set; }
        public string LicenseTypeId { get; set; }
        public string NamedLicenseId { get; set; }
        public string GroupNameId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public string DateAdded { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ConnectionString { get; set; }

        public IEnumerable<SelectListItem> CompanyList { get; set; }
        public IEnumerable<SelectListItem> GroupNameList { get; set; }
        public IEnumerable<SelectListItem> NamedLicenseList { get; set; }
        public IEnumerable<SelectListItem> LicenseTypeList { get; set; }
        public string TableData { get; set; }
    }
}
