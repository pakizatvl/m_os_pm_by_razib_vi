﻿namespace Keywest.Models
{
    public class CompanyEntity
    {
        public string Id { get; set; }
        public string CompanyName { get; set; }
    }
}