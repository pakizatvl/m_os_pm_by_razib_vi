﻿namespace KeywestCore
{
    public class SiteInfo
    {
        public string LoginUser { get; set; }
        public string LoginPass { get; set; }

        public string MenuId { get; set; }
        public string MenuIcon { get; set; }
        public string MenuName { get; set; }
        public string MenuUrl { get; set; }

        public bool Success { get; set; }
        public string Message { get; set; }
        public string TableData { get; set; }
        public string TabData { get; set; }
    }
}
