﻿namespace KeywestCore
{
    public class ConnectionString
    {
        public ConnectionString()
        {
        }
        public string DefaultConnection { get; set; }
        public string MainDBConnectionString { get; set; }
    }
}
