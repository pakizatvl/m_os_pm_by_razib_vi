﻿using Microsoft.Extensions.Configuration;

namespace KeywestCore
{
    public class ConnectionFactory
    {
        private static IConfiguration _config;
        public ConnectionFactory(IConfiguration config)
        {
            _config = config;
        }
        public string GetConnection()
        {
            //return _config.GetConnectionString("ConnectionStrings:DefaultConnection");
            return _config["ConnectionStrings:DefaultConnection"];
        }
    }
}
