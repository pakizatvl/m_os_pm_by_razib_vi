﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using Keywest.Structure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace KeywestCore.Controllers
{
    public class BaseController : Controller
    {
        //public object ExecuteDB(string methodName, object param)
        //{
        //    object retObject = ServerManager.GetTaskManager.Execute(methodName, param);
        //    return retObject;
        //}

        public object ExecuteDB(string methodName, object param)
        {
            object retObject = ServerManager.GetTaskManager.Execute(methodName, param);
            return retObject;
        }

        #region Site Functions
        public string SiteMainMenuList(string cPageHead = "", string cController = "", string cAction = "")
        {
            string IsAdd = "0"; //->    0/1 if access will confirm
            string IsEdit = "0"; //->   0/2 if access will confirm
            string IsDelete = "0"; //-> 0/3 if access will confirm
            bool IsAdmin = HttpContext.Session.GetString("UserType") != null && HttpContext.Session.GetString("UserType") == "Admin" ? true : false;
            //List<CsRolesdetailsEntity> rDetl = new List<CsRolesdetailsEntity>();
            //if (!IsAdmin)
            //{
            //    string RoleId = HttpContext.Session.GetString("UserRoleId");
            //    if (!string.IsNullOrEmpty(RoleId))
            //    {
            //        CsRolesdetailsEntity obj = new CsRolesdetailsEntity();
            //        obj.RoleId = RoleId;
            //        obj.IsDeactivate = "1";
            //        DataTable aDt = (DataTable)ExecuteDB(PTask.AG_GetAllCsRolesdetailsRecord, obj);
            //        foreach (DataRow dr in aDt.Rows)
            //            rDetl.Add(new CsRolesdetailsEntity { FeatureId = dr["FeatureId"].ToString(), IsView = dr["isView"].ToString(), IsAdd = dr["isAdd"].ToString(), IsEdit = dr["isEdit"].ToString(), IsDelete = dr["isDelete"].ToString() });
            //    }
            //}

            string cClass = cAction == "Dashboard" ? "active" : "", BcFirst = cPageHead, BcSecond = "";
            string SiteMenuList = "<ul class='navbar-nav'>"
                + "<li class='nav-item " + cClass + "'><a class='nav-link' href='/Home/Dashboard'><span class='active-item-here'></span><i class='fas fa-tachometer-alt mr-5'></i><span>Dashboard</span></a></li>";

            //..... Get Main Menu List
            List<SiteInfo> MainList = SiteMainFeaturesList();
            foreach (SiteInfo Menu in MainList)
            {
                bool iView = IsAdmin, iAdd = IsAdmin, iEdit = IsAdmin, iDelete = IsAdmin;
                //if (!IsAdmin && rDetl.Count > 0)
                //{
                //    var aDr = rDetl.FirstOrDefault(t => t.FeatureId == Menu.MenuId);
                //    if (aDr != null)
                //    {
                //        iView = aDr.IsView == "0" ? false : true;
                //        iAdd = aDr.IsAdd == "0" ? false : true;
                //        iEdit = aDr.IsEdit == "0" ? false : true;
                //        iDelete = aDr.IsDelete == "0" ? false : true;
                //    }
                //}

                if (iView)
                {
                    string mID = Menu.MenuId,
                        mName = Menu.MenuName,
                        mIcon = Menu.MenuIcon,
                        mUrl = Menu.MenuUrl;
                    string[] Url = mUrl.Split("/");
                    string Controller = Url.Length > 1 ? Url[1] : "", Action = Url.Length > 2 ? Url[2] : "";

                    //..... Get Sub Menu List
                    List<SiteInfo> SubList = SiteSubFeaturesList(mID);
                    if (SubList.Count > 0)
                    {
                        string SubMenuList = ""; cClass = "";
                        foreach (SiteInfo Sub in SubList)
                        {
                            string sName = Sub.MenuName, sUrl = Sub.MenuUrl;

                            Url = sUrl.Split("/"); Controller = Url.Length > 1 ? Url[1] : ""; Action = Url.Length > 2 ? Url[2] : "";
                            if (cController == Controller && cAction == Action)
                            {
                                cClass = "active";
                                BcFirst = mName;   BcSecond = sName;
                            }
                            SubMenuList += "<li class='nav-item'><a class='nav-link' href='" + sUrl + "'>" + sName + "</a></li>";

                            if (!string.IsNullOrEmpty(cClass) && iAdd) IsAdd = "1";
                            if (!string.IsNullOrEmpty(cClass) && iEdit) IsEdit = "2";
                            if (!string.IsNullOrEmpty(cClass) && iDelete) IsDelete = "3";
                        }

                        SiteMenuList += "<li class='nav-item dropdown " + cClass + "'><a class='nav-link dropdown-toggle' href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
                        if (Menu.Success) SiteMenuList += "<span class='active-item-here'></span><i class='" + mIcon + " mr-5'></i>";
                        else SiteMenuList += "<i class='mr-5'><img src='/rkScripts/images/" + mIcon + ".png' alt=''><img src='/rkScripts/images/" + mIcon + "-h.png' class='dis-no' alt=''></i>";
                        SiteMenuList += "<span>" + mName + "</span></a><ul class='dropdown-menu multilevel scale-up-left'>" + SubMenuList + "</ul></li>";
                    }
                    else
                    {
                        cClass = cController == Controller && cAction == Action ? "active" : "";
                        SiteMenuList += "<li class='nav-item " + cClass + "'><a class='nav-link' href='" + mUrl + "'>";
                        if (Menu.Success) SiteMenuList += "<span class='active-item-here'></span><i class='" + mIcon + " mr-5'></i>";
                        else SiteMenuList += "<i class='mr-5'><img src='/rkScripts/images/" + mIcon + ".png' alt=''><img src='/rkScripts/images/" + mIcon + "-h.png' class='dis-no' alt=''></i>";
                        SiteMenuList += "<span>" + mName + "</span></a></li>";

                        if (!string.IsNullOrEmpty(cClass) && iAdd) IsAdd = "1";
                        if (!string.IsNullOrEmpty(cClass) && iEdit) IsEdit = "2";
                        if (!string.IsNullOrEmpty(cClass) && iDelete) IsDelete = "3";
                        if (!string.IsNullOrEmpty(cClass)) { BcFirst = mName; BcSecond = ""; }
                    }
                }
            }
            SiteMenuList += "</ul>";
            ViewBag.SiteMenuMain = SiteMenuList;


            //...................... Site Breadcrumb
            if (!string.IsNullOrEmpty(ViewBag.BcMenu)) { BcFirst = ViewBag.BcMenu; BcSecond = cPageHead; }
            string Breadcrumb = "<section class='content-header'><h1>" + cPageHead + "</h1><ol class='breadcrumb'><li class='breadcrumb-item'><a href='/Home/Dashboard'>Home</a></li>";
            if (!string.IsNullOrEmpty(BcFirst)) Breadcrumb += "<li class='breadcrumb-item active'>" + BcFirst + "</li>";
            if (!string.IsNullOrEmpty(BcSecond)) Breadcrumb += "<li class='breadcrumb-item active'>" + BcSecond + "</li>";
            Breadcrumb += "</ol></section>";
            ViewBag.SiteBreadcrumb = Breadcrumb;


            return IsAdd + IsEdit + IsDelete;
            //bool Permission_Add = Access.Contains("1"), Permission_Edit = Access.Contains("2"), Permission_Delete = Access.Contains("3");
        }

        public List<SiteInfo> SiteMainFeaturesList()
        {
            List<SiteInfo> Items = new List<SiteInfo>();
            Items.Add(new SiteInfo { MenuId = "1", MenuIcon = "procurment", MenuName = "Procurement", MenuUrl = "#", Success = false });
            Items.Add(new SiteInfo { MenuId = "2", MenuIcon = "document-control", MenuName = "Document Control", MenuUrl = "#", Success = false });
            Items.Add(new SiteInfo { MenuId = "3", MenuIcon = "cost-control", MenuName = "Cost Control", MenuUrl = "#", Success = false });
            Items.Add(new SiteInfo { MenuId = "4", MenuIcon = "fas fa-coins", MenuName = "Expediting", MenuUrl = "#", Success = true });
            Items.Add(new SiteInfo { MenuId = "5", MenuIcon = "fas fa-user-tie", MenuName = "Client Management", MenuUrl = "#", Success = true });
            Items.Add(new SiteInfo { MenuId = "6", MenuIcon = "fas fa-sliders-h", MenuName = "Settings", MenuUrl = "#", Success = true });
            return Items;
        }
        public List<SiteInfo> SiteSubFeaturesList(string MenuId = "")
        {
            List<SiteInfo> Items = new List<SiteInfo>();
            if (MenuId == "1")
            {
                Items.Add(new SiteInfo { MenuName = "RFQ", MenuUrl = "/Procurement/RFQ" });
                Items.Add(new SiteInfo { MenuName = "Online Bidding", MenuUrl = "/Procurement/OnlineBidding" });
                Items.Add(new SiteInfo { MenuName = "Commitment", MenuUrl = "/Procurement/Commitment" });
                Items.Add(new SiteInfo { MenuName = "Change Order", MenuUrl = "/Procurement/ChangeOrder" });
                Items.Add(new SiteInfo { MenuName = "Progress Invoice", MenuUrl = "/Procurement/ProgressInvoice" });
                Items.Add(new SiteInfo { MenuName = "Reports", MenuUrl = "/Procurement/Reports" });
            }
            else if (MenuId == "2")
            {
                Items.Add(new SiteInfo { MenuName = "Documents", MenuUrl = "/Document/Documents" });
                Items.Add(new SiteInfo { MenuName = "Transmittals", MenuUrl = "/Document/Transmittals" });
                Items.Add(new SiteInfo { MenuName = "Squad Check", MenuUrl = "/Document/SquadCheck" });
                Items.Add(new SiteInfo { MenuName = "Reports", MenuUrl = "/Document/Reports" });
            }
            else if (MenuId == "3")
            {
                Items.Add(new SiteInfo { MenuName = "Estimate", MenuUrl = "/Cost/Estimate" });
                Items.Add(new SiteInfo { MenuName = "Budgets", MenuUrl = "/Cost/Budgets" });
                Items.Add(new SiteInfo { MenuName = "EAC", MenuUrl = "/Cost/EAC" });
                Items.Add(new SiteInfo { MenuName = "Cost Reports", MenuUrl = "/Cost/CostReports" });
                Items.Add(new SiteInfo { MenuName = "Field Tickets", MenuUrl = "/Cost/FieldTickets" });
                Items.Add(new SiteInfo { MenuName = "Reports", MenuUrl = "/Cost/Reports" });
            }
            else if (MenuId == "4")
            {
                Items.Add(new SiteInfo { MenuName = "Expedite", MenuUrl = "/Expediting/Expedite" });
                Items.Add(new SiteInfo { MenuName = "Expediting Report", MenuUrl = "/Expediting/ExpeditingReport" });
                Items.Add(new SiteInfo { MenuName = "MISC", MenuUrl = "/Expediting/MISC" });
            }
            else if (MenuId == "5")
            {
                Items.Add(new SiteInfo { MenuName = "Programmes", MenuUrl = "/Client/Programmes" });
                Items.Add(new SiteInfo { MenuName = "Projects", MenuUrl = "/Client/Projects" });
                Items.Add(new SiteInfo { MenuName = "Companies", MenuUrl = "/Client/Companies" });
                Items.Add(new SiteInfo { MenuName = "Cost Code", MenuUrl = "/Client/CostCode" });
                Items.Add(new SiteInfo { MenuName = "Reports", MenuUrl = "/Client/Reports" });
                Items.Add(new SiteInfo { MenuName = "Protfolio", MenuUrl = "/Client/Protfolio" });
            }
            else if (MenuId == "6")
            {
                Items.Add(new SiteInfo { MenuName = "Site Settings", MenuUrl = "/Settings/SiteSettings" });
                Items.Add(new SiteInfo { MenuName = "Group Permissions", MenuUrl = "/Settings/GroupPermissions" });
                Items.Add(new SiteInfo { MenuName = "User Settings", MenuUrl = "/Settings/UserSettings" });
                Items.Add(new SiteInfo { MenuName = "Work Flow", MenuUrl = "/Settings/WorkFlow" });
                Items.Add(new SiteInfo { MenuName = "CC Library", MenuUrl = "/Settings/CCLibrary" });
                Items.Add(new SiteInfo { MenuName = "Misc. Fields", MenuUrl = "/Settings/MiscFields" });
                Items.Add(new SiteInfo { MenuName = "Lists", MenuUrl = "/Settings/Lists" });
                Items.Add(new SiteInfo { MenuName = "Audit", MenuUrl = "/Settings/Audit" });
                Items.Add(new SiteInfo { MenuName = "Reports", MenuUrl = "/Settings/Reports" });
            }

            return Items;
        }

        //public bool SiteUserAccess(string MenuId, string Action)
        //{
        //    bool Access = false;
        //    bool IsAdmin = HttpContext.Session.GetString("UserType") == "Admin" ? true : false;
        //    if (!IsAdmin && "VAED".Contains(Action.ToUpper()))
        //    {
        //        string RoleId = HttpContext.Session.GetString("UserRoleId");
        //        CsRolesdetailsEntity obj = new CsRolesdetailsEntity();
        //        obj.RoleId = RoleId;
        //        obj.FeatureId = MenuId;
        //        obj.IsDeactivate = "1";
        //        if (Action.ToUpper() == "V") obj.IsView = "1";
        //        if (Action.ToUpper() == "A") obj.IsAdd = "1";
        //        if (Action.ToUpper() == "E") obj.IsEdit = "1";
        //        if (Action.ToUpper() == "D") obj.IsDelete = "1";
        //        DataTable dt = (DataTable)ExecuteDB(PTask.AG_GetAllCsRolesdetailsRecord, obj);
        //        if (dt.Rows.Count > 0) Access = true;
        //    }
        //    else if (IsAdmin) Access = true;
        //    return Access;
        //}
        #endregion


        #region Common Functions
        public JsonResult UploadFiles(string Folder = "")
        {
            if (Folder == "11") Folder = "Products";
            else if (Folder == "101") Folder = "Handling";
            else if (Folder == "55") Folder = "Users";
            else Folder = "";

            SiteInfo Result = new SiteInfo { Success = false };
            if (Request.Form.Files.Count > 0 && !string.IsNullOrEmpty(Folder))
            {
                int cSl = 1;
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                foreach (IFormFile ImageFile in Request.Form.Files)
                {
                    string NameKey = "name" + cSl;
                    string FileName = dict.ContainsKey(NameKey) && dict[NameKey] != "undefined" ? dict[NameKey] : Guid.NewGuid().ToString().ToLower();
                    var FilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Docx", Folder, FileName);
                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.File.Delete(FilePath);
                        string[] arr = FileName.Split('.');
                        FileName = arr[0];
                    }

                    //var FileName = ContentDispositionHeaderValue.Parse(ImageFile.ContentDisposition).FileName.Trim('"');
                    var FileExt = System.IO.Path.GetExtension(ImageFile.FileName);
                    FileName = FileName + FileExt;
                    FilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Docx", Folder, FileName);
                    if (System.IO.File.Exists(FilePath)) System.IO.File.Delete(FilePath);
                    using (System.IO.Stream stream = new FileStream(FilePath, FileMode.Create))
                    {
                        ImageFile.CopyToAsync(stream);
                    }
                    if (System.IO.File.Exists(FilePath))
                    {
                        Result.Success = true;
                        Result.TabData = "/Docx/" + Folder + "/" + FileName;
                    }
                }
            }

            Result.Message = Result.Success ? "File Uploaded" : "Sorry File Error!";
            return Json(JsonConvert.SerializeObject(Result));
        }

        public bool RkIsEmailValid(string emailaddress)
        {
            if (string.IsNullOrEmpty(emailaddress)) return false;
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public string DateFormatWebToDb(string dt)
        {
            string res = "";
            if (!string.IsNullOrEmpty(dt))
            {
                try
                {
                    DateTime dbTime = DateTime.ParseExact(dt, "yyyy-MM-dd", null);
                    res = dbTime.ToString("dd/MM/yyyy");
                }
                catch { }
            }
            return res;
        }

        public string DateFormatDbToWeb(string dt)
        {
            string res = "";
            if (!string.IsNullOrEmpty(dt))
            {
                try
                {
                    DateTime dbTime = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
                    res = dbTime.ToString("yyyy-MM-dd");
                }
                catch { }
            }
            return res;
        }


        public IEnumerable<SelectListItem> GetGroupNameList()
        {
            List<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem { Text = "Group Name 1", Value = "1" },
                new SelectListItem { Text = "Group Name 2", Value = "2" }
            };
            return items;
        }

        public IEnumerable<SelectListItem> GetCompanyNameList()
        {
            List<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem { Text = "Company Name 1", Value = "1" },
                new SelectListItem { Text = "Company Name 2", Value = "2" }
            };
            return items;
        }

        public IEnumerable<SelectListItem> GetLicenseTypeList()
        {
            List<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem { Text = "Full", Value = "1" },
                new SelectListItem { Text = "Guest", Value = "2" }
            };
            return items;
        }
        public IEnumerable<SelectListItem> GetNamedLicenseList()
        {
            List<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem { Text = "Named", Value = "1" },
                new SelectListItem { Text = "Concurrent", Value = "2" }
            };
            return items;
        }



        #endregion

    }
}