﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KeywestCore.Controllers
{
    public class CostController : BaseController
    {
        #region Estimate
        public IActionResult Estimate()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Estimate", "Cost", "Estimate");

            return View();
        }
        #endregion



        #region Budgets
        public IActionResult Budgets()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Budgets", "Cost", "Budgets");

            return View();
        }
        #endregion



        #region EAC
        public IActionResult EAC()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("EAC", "Cost", "EAC");

            return View();
        }
        #endregion



        #region CostReports
        public IActionResult CostReports()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Cost Reports", "Cost", "CostReports");

            return View();
        }
        #endregion



        #region FieldTickets
        public IActionResult FieldTickets()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Field Tickets", "Cost", "FieldTickets");

            return View();
        }
        #endregion



        #region Reports
        public IActionResult Reports()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Reports", "Cost", "Reports");

            return View();
        }
        #endregion
    }
}