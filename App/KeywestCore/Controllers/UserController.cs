﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KeywestCore.Controllers
{
    public class UserController : BaseController
    {
        #region UserProfile
        public IActionResult UserProfile()
        {
            ViewBag.BcMenu = "Profile";
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("View", "User", "UserProfile");

            return View();
        }
        #endregion



        #region ChangePassword
        public IActionResult ChangePassword()
        {
            ViewBag.BcMenu = "Profile";
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Change Password", "User", "ChangePassword");

            return View();
        }
        #endregion



        #region EditProfile
        public IActionResult EditProfile()
        {
            ViewBag.BcMenu = "Profile";
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Edit", "User", "EditProfile");

            return View();
        }
        #endregion



        #region Delegate
        public IActionResult Delegate()
        {
            ViewBag.BcMenu = "Profile";
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Delegate", "User", "Delegate");

            return View();
        }
        #endregion
    }
}