﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KeywestCore.Controllers
{
    public class ClientController : BaseController
    {
        #region Programmes
        public IActionResult Programmes()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Programmes", "Client", "Programmes");

            return View();
        }
        #endregion



        #region Projects
        public IActionResult Projects()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Projects", "Client", "Projects");

            return View();
        }
        #endregion



        #region Companies
        public IActionResult Companies()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Companies", "Client", "Companies");

            return View("Programmes");
        }
        #endregion



        #region CostCode
        public IActionResult CostCode()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Cost Code", "Client", "CostCode");

            return View();
        }
        #endregion



        #region Reports
        public IActionResult Reports()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Reports", "Client", "Reports");

            return View();
        }
        #endregion



        #region Protfolio
        public IActionResult Protfolio()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Protfolio", "Client", "Protfolio");

            return View();
        }
        #endregion
    }
}