﻿using System;
using System.Collections.Generic;
using System.Data;
using Keywest.Models;
using Keywest.Structure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace KeywestCore.Controllers
{
    public class SettingsController : BaseController
    {
        public IConfiguration Configuration { get; }
        private readonly ConnectionString _connectionstring;
        private readonly ConnectionFactory connectionFactory;
        public SettingsController(IConfiguration configuration,
            ConnectionFactory connectionFactory,
            IOptions<ConnectionString> connectionstringAccessor)
        {
            Configuration = configuration;
            _connectionstring = connectionstringAccessor.Value;
            this.connectionFactory = connectionFactory;
        }

        #region SiteSettings
        public IActionResult SiteSettings()
        {
            if (HttpContext.Session.GetString("UserId") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.uAccess = SiteMainMenuList("Site Settings", "Settings", "SiteSettings");

            return View();
        }

        #endregion


        #region GroupPermissions
        public IActionResult GroupPermissions()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Group Permissions", "Settings", "GroupPermissions");

            return View();
        }
        #endregion



        #region UserSettings
        public IActionResult UserSettings()
        {
            if (HttpContext.Session.GetString("UserId") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.uAccess = SiteMainMenuList("User Settings", "Settings", "UserSettings");

            UserEntity objUser = new UserEntity();
            objUser.GroupNameList = GetGroupNameList();
            objUser.CompanyList = GetCompanyNameList();
            objUser.LicenseTypeList = GetLicenseTypeList();
            objUser.NamedLicenseList = GetNamedLicenseList();
            objUser.TableData = GetTableDataForUsersList(objUser);

            return View(objUser);
        }

        [HttpPost]
        public JsonResult AddUpdateUserSettings(UserEntity userEntity)
        {
            try
            {
                string connectionString = Configuration["ConnectionStrings:DefaultConnection"];
                string result = connectionFactory.GetConnection();
                bool Success = false;
                string Message = "Sorry, Time Out!";
                if (HttpContext.Session.GetString("UserId") != null)
                {
                    Message = "Sorry something went wrong!";
                    userEntity.UpdatedBy = HttpContext.Session.GetString("UserId");
                    userEntity.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    userEntity.ConnectionString = connectionString;

                    if (string.IsNullOrEmpty(userEntity.Id))
                    {
                        userEntity.CreatedBy = HttpContext.Session.GetString("UserId");
                        userEntity.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        Success = (bool)ExecuteDB(PTask.AG_SaveUserInfo, userEntity);
                    }
                    else
                    {
                        Success = (bool)ExecuteDB(PTask.AG_UpdateUserInfo, userEntity);
                    }
                }
                if (Success)
                {
                    Message = "Process has been done successfully.";
                }
                return Json(new { Success = Success, Message = Message });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult GetAllUsersList(UserEntity userEntity)
        {
            try
            {
                bool Success = true;
                userEntity.TableData = GetTableDataForUsersList(userEntity);
                return Json(new { Success = Success, TableData = userEntity.TableData });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        private string GetTableDataForUsersList(UserEntity userEntity)
        {
            string TableData = "";
            string tHead = "";
            int icount = 0;
            string connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            userEntity.ConnectionString = connectionString;
            var dt = ExecuteDB(PTask.AG_GetAllUserRecord, userEntity) as DataTable;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    TableData +=

                               "<tr>"

                               + "<td>" + dr["Image"].ToString() + "</td>"
                               + "<td>" + dr["ID"].ToString() + "</td>"
                               + "<td>" + dr["Contact"].ToString() + "</td>"
                               + "<td>" + dr["FirstName"].ToString() + "</td>"
                               + "<td>" + dr["LastName"].ToString() + "</td>"
                               + "<td>" + dr["CompanyID"].ToString() + "</td>"
                               + "<td>" + dr["LicenseTypeID"].ToString() + "</td>"
                               + "<td>" + dr["NamedLicenseID"].ToString() + "</td>"
                               + "<td>" + dr["GroupNameID"].ToString() + "</td>"
                               + "<td>" + dr["Password"].ToString() + "</td>"
                               + "<td>" + dr["Email"].ToString() + "</td>"

                               + "</tr>";
                    icount += 1;
                }
            }

            return TableData;
        }

        public IActionResult UserSettings1()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("User Settings", "Settings", "UserSettings");

            return View();
        }
        #endregion



        #region WorkFlow
        public IActionResult WorkFlow()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Work Flow", "Settings", "WorkFlow");

            return View();
        }
        #endregion



        #region CCLibrary
        public IActionResult CCLibrary()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("CC Library", "Settings", "CCLibrary");

            return View();
        }
        #endregion



        #region MiscFields
        public IActionResult MiscFields()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Misc. Fields", "Settings", "MiscFields");

            return View();
        }
        #endregion



        #region Lists
        public IActionResult Lists()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Lists", "Settings", "Lists");

            return View();
        }
        #endregion



        #region Audit
        public IActionResult Audit()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Audit", "Settings", "Audit");

            return View();
        }
        #endregion



        #region Reports
        public IActionResult Reports()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Reports", "Settings", "Reports");

            return View();
        }
        #endregion
    }
}