﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using KeywestCore.Models;
using Microsoft.AspNetCore.Http;
using Keywest.Structure;
using System.Data;

namespace KeywestCore.Controllers
{
    public class HomeController : BaseController
    {
        #region LogIn / LogOut
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("LogedMessage") != null)
            {
                ViewBag.LogedMessage = HttpContext.Session.GetString("LogedMessage");
                HttpContext.Session.Remove("LogedMessage");
            }

            //.... Demo
            SiteInfo obj = new SiteInfo();
            obj.LoginUser = "admin";
            obj.LoginPass = "@dmiN1234";

            return View(obj);
        }

        [HttpPost]
        public IActionResult Index(SiteInfo obj)
        {
            if (string.IsNullOrEmpty(obj.LoginUser))
            {
                ViewBag.LogedMessage = "Sorry, Empty Username";
            }
            else if (string.IsNullOrEmpty(obj.LoginPass))
            {
                ViewBag.LogedMessage = "Sorry, Empty Password";
            }
            else
            {
                //......... Demo Initilizations Start
                HttpContext.Session.SetString("UserId", "1");
                HttpContext.Session.SetString("UserType", "Admin");
                HttpContext.Session.SetString("UserName", "Admin");
                HttpContext.Session.SetString("UserRoleId", "1");
                return RedirectToAction("Dashboard", "Home");
                //......... Demo Initilizations End


                //CsUsersEntity uObj = new CsUsersEntity();
                //uObj.Username = obj.LoginUser;
                //uObj.Userpass = obj.LoginPass;
                //uObj.Activestatus = "1";
                //DataTable uDt = (DataTable)ExecuteDB(PTask.AG_GetAllCsUsersRecord, uObj);
                //if (uDt.Rows.Count == 1)
                //{
                //    uObj.Id = uDt.Rows[0]["Id"].ToString();
                //    uObj.LastLoginTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                //    ExecuteDB(PTask.AG_UpdateCsUsersInfo, uObj);

                //    HttpContext.Session.SetString("UserId", uDt.Rows[0]["Id"].ToString());
                //    HttpContext.Session.SetString("UserType", uDt.Rows[0]["UserType"].ToString());
                //    HttpContext.Session.SetString("UserName", uDt.Rows[0]["Title"] + " " + uDt.Rows[0]["FirstName"] + " " + uDt.Rows[0]["LastName"]);
                //    HttpContext.Session.SetString("UserRoleId", uDt.Rows[0]["RoleId"].ToString());
                //    HttpContext.Session.SetString("CompanyName", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                //    UpdateLogInformation(1);
                //    return RedirectToAction("Dashboard", "Home");
                //}
                //else if (obj.LoginUser.ToUpper() == "ADMIN" && obj.LoginPass == "admin")
                //{
                //    ViewBag.LogedMessage = "Sorry something went wrong!";
                //    //var UserId = HttpContext.Session.GetString("UserId");
                //    UpdateLogInformation(3);

                //    //......... Demo Initilizations Start
                //    HttpContext.Session.SetString("UserId", "1");
                //    HttpContext.Session.SetString("UserType", "Admin");
                //    HttpContext.Session.SetString("UserName", "Admin");
                //    HttpContext.Session.SetString("CompanyName", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                //    HttpContext.Session.SetString("UserRoleId", "1");
                //    return RedirectToAction("Dashboard", "Home");
                //    //......... Demo Initilizations End
                //}
            }

            return View(obj);
        }

        public IActionResult LogOut()
        {
            HttpContext.Session.Clear();
            HttpContext.Session.SetString("LogedMessage", "Logged Out");
            return RedirectToAction("Index", "Home");
        }
        #endregion



        #region Dashboard Area
        public IActionResult Dashboard()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Dashboard", "Home", "Dashboard");

            DataTable dt = (DataTable)ExecuteDB(PTask.TestFinctions, null);

            return View();
        }
        #endregion

        




        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
