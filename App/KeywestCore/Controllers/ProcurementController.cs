﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KeywestCore.Controllers
{
    public class ProcurementController : BaseController
    {
        #region RFQ
        public IActionResult RFQ()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("RFQ", "Procurement", "RFQ");

            return View();
        }
        #endregion



        #region OnlineBidding
        public IActionResult OnlineBidding()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Online Bidding", "Procurement", "OnlineBidding");

            return View();
        }
        #endregion



        #region Commitment
        public IActionResult Commitment()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Commitment", "Procurement", "Commitment");

            return View();
        }
        #endregion



        #region ChangeOrder
        public IActionResult ChangeOrder()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Change Order", "Procurement", "ChangeOrder");

            return View();
        }
        #endregion



        #region ProgressInvoice
        public IActionResult ProgressInvoice()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Progress Invoice", "Procurement", "ProgressInvoice");

            return View();
        }
        #endregion



        #region Reports
        public IActionResult Reports()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Reports", "Procurement", "Reports");

            return View();
        }
        #endregion
    }
}