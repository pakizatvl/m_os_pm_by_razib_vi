﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KeywestCore.Controllers
{
    public class ExpeditingController : BaseController
    {
        #region Expedite
        public IActionResult Expedite()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Expedite", "Expediting", "Expedite");

            return View();
        }
        #endregion



        #region ExpeditingReport
        public IActionResult ExpeditingReport()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Expediting Report", "Expediting", "ExpeditingReport");

            return View();
        }
        #endregion



        #region MISC
        public IActionResult MISC()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("MISC", "Expediting", "MISC");

            return View();
        }
        #endregion
    }
}