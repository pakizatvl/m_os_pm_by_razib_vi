﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KeywestCore.Controllers
{
    public class DocumentController : BaseController
    {
        #region Documents
        public IActionResult Documents()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Documents", "Document", "Documents");

            return View();
        }
        #endregion



        #region Transmittals
        public IActionResult Transmittals()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Transmittals", "Document", "Transmittals");

            return View();
        }
        #endregion



        #region SquadCheck
        public IActionResult SquadCheck()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Squad Check", "Document", "SquadCheck");

            return View();
        }
        #endregion



        #region Reports
        public IActionResult Reports()
        {
            if (HttpContext.Session.GetString("UserId") == null) return RedirectToAction("Index", "Home");
            ViewBag.uAccess = SiteMainMenuList("Reports", "Document", "Reports");

            return View();
        }
        #endregion
    }
}