﻿namespace KeywestCore.Models
{
    public class User
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string LicenseType { get; set; }
        public string NamedLicense { get; set; }
        public string GroupName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
