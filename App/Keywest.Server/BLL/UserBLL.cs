using Keywest.Models;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Keywest.Server.DAL;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace Keywest.Server.BLL
{
    public partial class UserBLL
    {
        #region Auto Generated

        public object SaveUserInfo(object param)
        {
            UserEntity userEntity = (UserEntity)param;
            object retObj = null;
            using (SqlConnection db = new SqlConnection(userEntity.ConnectionString))
            {
                db.Open();
                DbTransaction transaction = db.BeginTransaction();
                try
                {

                    UserDALPartial userDAL = new UserDALPartial();
                    retObj = (object)userDAL.SaveUserInfo(userEntity, db, transaction);
                    //transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    db.Close();
                }
            }


            //Database db = DatabaseFactory.CreateDatabase();
            //using (DbConnection connection = db.CreateConnection())
            //{
            //    connection.Open();
            //    DbTransaction transaction = connection.BeginTransaction();
            //    try
            //    {

            //        UserDAL userDAL = new UserDAL();
            //        retObj = (object)userDAL.SaveUserInfo(userEntity, db, transaction);
            //        transaction.Commit();
            //    }
            //    catch
            //    {
            //        transaction.Rollback();
            //        throw;
            //    }
            //    finally
            //    {
            //        connection.Close();
            //    }
            //}

            return retObj;
        }

        public object UpdateUserInfo(object param)
        {
            Database db = DatabaseFactory.CreateDatabase();
            object retObj = null;
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                try
                {
                    UserEntity userEntity = (UserEntity)param;
                    UserDALPartial userDAL = new UserDALPartial();
                    retObj = (object)userDAL.UpdateUserInfo(userEntity, db, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            return retObj;
        }

        public object DeleteUserInfoById(object param)
        {
            Database db = DatabaseFactory.CreateDatabase();
            object retObj = null;
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                try
                {
                    UserDALPartial userDAL = new UserDALPartial();
                    retObj = (object)userDAL.DeleteUserInfoById(param, db, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            return retObj;
        }

        public object GetSingleUserRecordById(object param)
        {
            object retObj = null;
            UserDALPartial userDAL = new UserDALPartial();
            retObj = (object)userDAL.GetSingleUserRecordById(param);
            return retObj;
        }

        #endregion

    }
}

