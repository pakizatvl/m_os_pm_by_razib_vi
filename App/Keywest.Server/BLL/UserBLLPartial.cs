using Keywest.Server.DAL;

namespace Keywest.Server.BLL
{
    public partial class UserBLL
	{		
		public object GetAllUserRecord(object param)
		{
			object retObj = null;
			UserDALPartial userDAL = new UserDALPartial();
			retObj = (object)userDAL.GetAllUserRecord(param);
			return retObj;
		}

	}
}

