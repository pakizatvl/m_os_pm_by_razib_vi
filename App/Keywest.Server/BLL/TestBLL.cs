﻿using Keywest.Server.DAL;

namespace Keywest.Server.BLL
{
    public class TestBLL
    {
        public object GetTestData(object param)
        {
            object retObj = null;
            TestDAL testDAL = new TestDAL();
            retObj = (object)testDAL.GetTestData(param);
            return retObj;
        }
    }
}
