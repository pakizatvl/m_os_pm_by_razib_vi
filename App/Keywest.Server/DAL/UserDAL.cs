using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Keywest.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Keywest.Server.DAL
{
    public partial class UserDALPartial
    {
        #region Auto Generated

        public bool SaveUserInfo(UserEntity userEntity, Database db, DbTransaction transaction)
        {
            string sql = "INSERT INTO User ( FirstName, LastName, CompanyId, LicenseTypeId, NamedLicenseId, GroupNameId, Password, Email) VALUES (  @Firstname,  @Lastname,  @LicenseTypeId, @NamedLicenseId, @GroupNameId,  @Password,  @Email)";

            DbCommand dbCommand = db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "FirstName", DbType.String, userEntity.FirstName);
            db.AddInParameter(dbCommand, "LastName", DbType.String, userEntity.LastName);
            db.AddInParameter(dbCommand, "CompanyId", DbType.String, userEntity.CompanyId);
            db.AddInParameter(dbCommand, "LicenseTypeId", DbType.String, userEntity.LicenseTypeId);
            db.AddInParameter(dbCommand, "NamedLicenseId", DbType.String, userEntity.NamedLicenseId);
            db.AddInParameter(dbCommand, "GroupNameId", DbType.String, userEntity.GroupNameId);
            db.AddInParameter(dbCommand, "Password", DbType.String, userEntity.Password);
            db.AddInParameter(dbCommand, "Email", DbType.String, userEntity.Email);

            db.ExecuteNonQuery(dbCommand, transaction);
            return true;
        }

        internal object SaveUserInfo(UserEntity userEntity, SqlConnection db, DbTransaction transaction)
        {
            string sql =
               $" Insert Into [User] ([FirstName], [LastName], [Contact], [CompanyId], [LicenseTypeId], [NamedLicenseId], [GroupNameId], [Password], [Image], [Email]) Values " +
               $"('{userEntity.FirstName}', '{userEntity.LastName}','{userEntity.Contact}','{userEntity.CompanyId}','{userEntity.LicenseTypeId}'," +
               $"'{userEntity.NamedLicenseId}','{userEntity.GroupNameId}','{userEntity.Password}','{userEntity.Image}','{userEntity.Email}')";

            //DbTransaction _transaction = db.BeginTransaction();
            using (SqlCommand command = new SqlCommand(sql, db, (SqlTransaction)transaction))
            {
                command.CommandType = CommandType.Text;
                //db.Open();
                command.ExecuteNonQuery();
                transaction.Commit();
                db.Close();
            }
            return true;
        }

        public bool UpdateUserInfo(UserEntity userEntity, Database db, DbTransaction transaction)
        {
            string sql = "UPDATE User SET FirstName= @Firstname, LastName= @Lastname, CompanyId= @CompanyId, LicenseTypeId= @LicenseTypeId, NamedLicenseId= @NamedLicenseId, GroupNameId= @GroupNameId, Password= @Password, Email= @Email WHERE Id=@Id";
            DbCommand dbCommand = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "Id", DbType.String, userEntity.Id);
            db.AddInParameter(dbCommand, "FirstName", DbType.String, userEntity.FirstName);
            db.AddInParameter(dbCommand, "LastName", DbType.String, userEntity.LastName);
            db.AddInParameter(dbCommand, "CompanyId", DbType.String, userEntity.CompanyId);
            db.AddInParameter(dbCommand, "LicenseTypeId", DbType.String, userEntity.LicenseTypeId);
            db.AddInParameter(dbCommand, "NamedLicenseId", DbType.String, userEntity.NamedLicenseId);
            db.AddInParameter(dbCommand, "GroupNameId", DbType.String, userEntity.GroupNameId);
            db.AddInParameter(dbCommand, "Password", DbType.String, userEntity.Password);
            db.AddInParameter(dbCommand, "Email", DbType.String, userEntity.Email);

            db.ExecuteNonQuery(dbCommand, transaction);
            return true;
        }

        public bool DeleteUserInfoById(object param, Database db, DbTransaction transaction)
        {
            string sql = "DELETE FROM User WHERE Id=@Id";
            DbCommand dbCommand = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "Id", DbType.String, param);

            db.ExecuteNonQuery(dbCommand, transaction);
            return true;
        }

        public UserEntity GetSingleUserRecordById(object param)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "SELECT [ID], [FirstName], [LastName], [CompanyId], [LicenseTypeId], [NamedLicenseId], [GroupNameId], [Password], [Image], [Email], [DateAdded] FROM [User] WHERE Id=@Id";
            DbCommand dbCommand = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "Id", DbType.String, param);
            UserEntity userEntity = null;
            using (IDataReader dataReader = db.ExecuteReader(dbCommand))
            {
                if (dataReader.Read())
                {
                    userEntity = new UserEntity();
                    if (dataReader["ID"] != DBNull.Value)
                    {
                        userEntity.Id = dataReader["ID"].ToString();
                    }
                    if (dataReader["FirstName"] != DBNull.Value)
                    {
                        userEntity.FirstName = dataReader["FirstName"].ToString();
                    }
                    if (dataReader["LastName"] != DBNull.Value)
                    {
                        userEntity.LastName = dataReader["LastName"].ToString();
                    }
                    if (dataReader["CompanyId"] != DBNull.Value)
                    {
                        userEntity.CompanyId = dataReader["CompanyId"].ToString();
                    }
                    if (dataReader["LicenseTypeId"] != DBNull.Value)
                    {
                        userEntity.LicenseTypeId = dataReader["LicenseTypeId"].ToString();
                    }
                    if (dataReader["NamedLicenseId"] != DBNull.Value)
                    {
                        userEntity.NamedLicenseId = dataReader["NamedLicenseId"].ToString();
                    }
                    if (dataReader["GroupNameId"] != DBNull.Value)
                    {
                        userEntity.GroupNameId = dataReader["GroupNameId"].ToString();
                    }
                    if (dataReader["Password"] != DBNull.Value)
                    {
                        userEntity.Password = dataReader["Password"].ToString();
                    }
                    if (dataReader["Image"] != DBNull.Value)
                    {
                        userEntity.Image = dataReader["Image"].ToString();
                    }
                    if (dataReader["Email"] != DBNull.Value)
                    {
                        userEntity.Email = dataReader["Email"].ToString();
                    }
                    if (dataReader["DateAdded"] != DBNull.Value)
                    {
                        userEntity.DateAdded = dataReader["DateAdded"].ToString();
                    }
                }
            }
            return userEntity;
        }

        #endregion

    }

    public class ConnectionFactory
    {
        private static IConfiguration _config;
        public ConnectionFactory(IConfiguration config)
        {
            _config = config;
        }
        public string GetConnection()
        {
            return _config.GetConnectionString("ConnectionStrings:DefaultConnection");
        }
    }

    public class BaseConnection
    {
        private static readonly IConfiguration _config;
        public ConnectionFactory _ConnectionFactory = null;

        public BaseConnection()
        {
            _ConnectionFactory = new ConnectionFactory(_config);
        }
    }
}

