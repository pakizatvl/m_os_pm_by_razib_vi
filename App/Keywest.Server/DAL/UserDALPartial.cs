using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Keywest.Models;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Keywest.Server.DAL
{
    public partial class UserDALPartial
    {
        public DataTable GetAllUserRecord(object param)
        {
            UserEntity objParam = (UserEntity)param;
            var dataTable = new DataTable();

            using (SqlConnection connection = new SqlConnection(objParam.ConnectionString))
            {
                connection.Open();
                string sql = "SELECT [ID], [FirstName], [LastName], [Contact], [CompanyId], [LicenseTypeId], [NamedLicenseId], [GroupNameId], [Password], [Email], [Image], [DateAdded] FROM [User]";
                SqlCommand command = new SqlCommand(sql, connection);
                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    dataTable.Load(dataReader);
                }
                connection.Close();

                // Database db = DatabaseFactory.CreateDatabase();
                //string sql = "SELECT ID, FirstName, LastName, CompanyId, LicenseTypeId, NamedLicenseId, GroupNameId, Password, Email, DateAdded FROM User";
                //DbCommand dbCommand = db.GetSqlStringCommand(sql);
                //DataSet ds = db.ExecuteDataSet(dbCommand);
                //return ds.Tables[0];
            }
            return dataTable;

        }
    }
}

