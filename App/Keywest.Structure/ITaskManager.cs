﻿namespace Keywest.Structure
{
    public interface ITaskManager
    {
        object Execute(string methodName, object param);
    }
}
