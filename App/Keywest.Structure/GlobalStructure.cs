﻿namespace Keywest.Structure
{
    public partial struct PTask
    {
        public const string TestFinctions = "TestFinctions";

		#region Auto Generated - User
		public const string AG_SaveUserInfo = "AG_SaveUserInfo";
		public const string AG_UpdateUserInfo = "AG_UpdateUserInfo";
		public const string AG_DeleteUserInfoById = "AG_DeleteUserInfoById";
		public const string AG_GetAllUserRecord = "AG_GetAllUserRecord";
		public const string AG_GetSingleUserRecordById = "AG_GetSingleUserRecordById";
		#endregion
	}
}
