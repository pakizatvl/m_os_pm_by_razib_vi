﻿using Keywest.Server.BLL;
using Microsoft.Extensions.Configuration;

namespace Keywest.Structure
{
    public class ServerObject : ITaskManager
    {
        //private string connectionString;
        //public IConfiguration Configuration { get; }
		public object Execute(string methodName, object param)
        {
			switch (methodName)
            {
                case PTask.TestFinctions:
                    TestBLL iTest = new TestBLL();
                    return iTest.GetTestData("");
                default:
                    break;

				#region Auto Generated - User
				case PTask.AG_SaveUserInfo:
					UserBLL userBLL = null;
					userBLL = new UserBLL();
					return userBLL.SaveUserInfo(param);
					break;
				case PTask.AG_UpdateUserInfo:
					userBLL = new UserBLL();
					return userBLL.UpdateUserInfo(param);
					break;
				case PTask.AG_DeleteUserInfoById:
					userBLL = new UserBLL();
					return userBLL.DeleteUserInfoById(param);
					break;
				case PTask.AG_GetAllUserRecord:
					userBLL = new UserBLL();
					return userBLL.GetAllUserRecord(param);
					break;
				case PTask.AG_GetSingleUserRecordById:
					userBLL = new UserBLL();
					return userBLL.GetSingleUserRecordById(param);
					break;
					#endregion
			}
			return null;
        }

    }
}
